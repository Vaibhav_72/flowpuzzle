package com.android.flowpuzzle;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

class SolverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solver);
        String[] validBoardWords = getIntent().getStringArrayExtra("validBoardWords");
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearLayout);
        for(String word : validBoardWords) {
            TextView wordText = new TextView(this);
            wordText.setText(word);
            linearLayout.addView(wordText);
        }
    }
    public void goMainActivity(View v) {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
